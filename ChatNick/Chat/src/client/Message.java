/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author gugle
 */
public class Message implements Serializable{
    private String nick, ip,mensaje , texto;
    List<String> conectados= new ArrayList();
    List<String> direccionIp= new ArrayList();

    public List<String> getConectados() {
        return conectados;
    }

    public void setConectados(List<String> conectados) {
        this.conectados = conectados;
    }

    public List<String> getDireccionIp() {
        return direccionIp;
    }

    public void setDireccionIp(List<String> direccionIp) {
        this.direccionIp = direccionIp;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

   

    public Message(String nick, String ip, String texto) {
        this.nick = nick;
        this.ip = ip;
        this.texto = texto;
    }

    public Message() {
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }
}
