/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

/**
 *
 * @author gugle
 */
import client.Message;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;

public class ClientInterface extends JFrame implements Runnable {
    private final JLabel lblTitle, lblClient, lblIp, lblMessage, lblMessages, txtClient;
    private final JTextField txtMessage;
    private final JTextArea txtMessages;
    private final JButton btnSubmit, btnLimpiar;
    private final JPanel panel;
    private final JComboBox user_conect;
    private List<String>Direcciones=new ArrayList();
    
    private Socket socketClient;
    
    public ClientInterface(String title){
        String nombre=JOptionPane.showInputDialog("Nombre: ");
        lblTitle    = new JLabel("                 Usuarios conectados");
        lblClient   = new JLabel("Nombre: ");
        lblIp       = new JLabel("             ");
        lblMessage  = new JLabel ("Mensaje: ");
        lblMessages = new JLabel ("-Chat:-");
        
        txtMessage  = new JTextField (25);
        txtMessages = new JTextArea (10, 25);
        txtClient   = new JLabel ();
        //txtIp       = new JTextField (25);
        user_conect = new JComboBox();
        
        btnSubmit   = new JButton("Enviar");
        btnLimpiar  = new JButton("Limpiar");
        
        btnSubmit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    int localizacion=user_conect.getSelectedIndex();
                    String cliente=Direcciones.get(localizacion);
                    socketClient = new Socket("192.168.1.6", 9999);
                    try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(socketClient.getOutputStream())) {
                        Message message = new Message(txtClient.getText(), 
                        cliente, txtMessage.getText());
                        txtMessages.append("\n" + txtMessage.getText());
                        objectOutputStream.writeObject(message);
                    }
                    socketClient.close();
                } catch (IOException ex) {
                    txtMessages.setText(txtMessages.getText() + "\n" + txtMessage.getText() + " - Message not sent.");
                    System.out.println(ex.getMessage());
                }  
            }
        });
        
        panel = new JPanel();
        txtClient.setText(nombre);
        panel.add(lblTitle);     panel.add(user_conect);
        panel.add(lblIp);        //panel.add(txtIp);
        panel.add(lblClient);    panel.add(txtClient);
        panel.add(lblMessages);  panel.add(txtMessages);
        panel.add(lblMessage);   panel.add(txtMessage);
        panel.add(btnSubmit);    panel.add(btnLimpiar);
        
        this.setBounds(800, 350, 325, 410);
        this.add(panel);
        this.setVisible(true);
        this.setTitle(title);
        Thread hilo=new Thread(this);
        hilo.start();
    }
    @Override
    public void run() {
        try {
            socketClient = new Socket("192.168.1.6", 9999);
                try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(socketClient.getOutputStream())) {
                    Message message = new Message();   
                    message.setNick(txtClient.getText());
                    message.setMensaje(" online");
                    objectOutputStream.writeObject(message);       
                }
                socketClient.close();
        } catch (IOException e) {
            txtMessages.setText(txtMessages.getText() + "\n" + txtMessage.getText() + " - Message not sent.");
                System.out.println(e.getMessage());
        }
        
        try {
            ServerSocket servidor=new ServerSocket(9090);
            Socket cliente;
            Message messagerecived;
             while(true){ 
                cliente=servidor.accept();
                 ObjectInputStream entradadatos= new ObjectInputStream(cliente.getInputStream());
                 messagerecived=(Message) entradadatos.readObject();
                 if(!(messagerecived.getMensaje().equals(" online"))){
                 txtMessages.append("\n"+ messagerecived.getNick() +": "+messagerecived.getTexto());
                 }
                 else
                 {
                    List<String> nombres_conect=new ArrayList();
                    Direcciones=messagerecived.getDireccionIp();
                    nombres_conect=messagerecived.getConectados();
                    user_conect.removeAllItems();
                    for(String i:nombres_conect){
                        user_conect.addItem(i);
                    }
                 }
            }
        } catch (IOException ex) {
            Logger.getLogger(ClientInterface.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ClientInterface.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
}
