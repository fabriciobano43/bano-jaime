/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serverSocket;

import client.Message;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import views.ServerInterface;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

public class MyThread extends Thread
{
    private ServerInterface serverInterface;
    
    public MyThread(String name, ServerInterface serverInterfaz){
        super(name);
        this.serverInterface = serverInterfaz;
        start();
    }

    @Override
    public void run() {
 ServerSocket serverSocket;
        List<String> conectados= new ArrayList();
        List<String> direcciones= new ArrayList();
        Socket server;
        try {
            serverSocket = new ServerSocket(9999);
            String IP,mensaje;
            Message receivedMessage;
            while (true)
            {
                try{
                    server = serverSocket.accept();
                    ObjectInputStream objectInputStream = new ObjectInputStream(server.getInputStream());
                    try {
                        
                        String nombre;
                        receivedMessage = (Message) objectInputStream.readObject();
                        IP=receivedMessage.getIp();
                        mensaje=receivedMessage.getMensaje();
                        nombre=receivedMessage.getNick();
                        if(!mensaje.equals(" online"))
                        {
                            serverInterface.getMenssages().append("\nNombre: " + receivedMessage.getNick() + " - Ip: " 
                                + receivedMessage.getIp() + " - Texto: " + receivedMessage.getTexto());
                            //reenviar paquete al destinatario
                         Socket cliente= new Socket(IP,9090);
                         ObjectOutputStream reenvio=new ObjectOutputStream(cliente.getOutputStream());
                         reenvio.writeObject(receivedMessage);
                         cliente.close();
                        }
                        else{
                            InetAddress adress = server.getInetAddress();
                            String IpRemota=adress.getHostAddress();
                            serverInterface.getMenssages().append("\n" + nombre +": ip: "+ IpRemota);
                            conectados.add(nombre);
                            direcciones.add(IpRemota);
                            receivedMessage.setConectados(conectados);
                            receivedMessage.setDireccionIp(direcciones);
                            for (String i:direcciones){
                             Socket cliente= new Socket(i,9090);
                             ObjectOutputStream reenvio=new ObjectOutputStream(cliente.getOutputStream());
                             reenvio.writeObject(receivedMessage);
                             cliente.close();
                            }
                        }
                         
                    } catch (ClassNotFoundException ex) {
                        Logger.getLogger(MyThread.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    server.close();
                }
                catch (IOException ex){
                    System.out.println (ex.getMessage());
                }
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }    
}

