
package views;

/**
 *
 * @author Fabricio
 */
import java.io.DataInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import javax.accessibility.AccessibleContext;
import javax.swing.*;
import static views.ServerInterface.txtMessages;

public final class ServerInterface extends JFrame /*implements Runnable*/ {
    private final JLabel lblTitle, lblMessages; /*, lblMessage*/
    //private final JTextField txtClient, txtIp; //txtMessage
    public static JTextArea txtMessages;
    private final JButton btnLimpiar; //btnSubmit,
    private final JPanel panel;
    //private final Thread myThread;
    private MyThread myThread;
    
    
    public JTextArea getMenssages(){
        return txtMessages;
    }
    
    public ServerInterface(String title){
        lblTitle    = new JLabel("Chat (Servidor)");
        lblMessages = new JLabel ("Chat: ");
        txtMessages = new JTextArea (10, 25);
        btnLimpiar  = new JButton("Limpiar");
        panel       = new JPanel();

        panel.add(lblTitle);
        panel.add(lblMessages);  panel.add(txtMessages);
        panel.add(btnLimpiar);
        
        this.setBounds(800, 350, 320, 410);
        this.add(panel);
        this.setVisible(true);
        this.setTitle(title);
        //listen();
        
        myThread = new MyThread("Hilo");
        myThread.start();
        
    }
    
  public void listen()
    {
        ServerSocket serverSocket;
        Socket server;
        try {
            serverSocket = new ServerSocket(5555);
            //System.out.println("Ya creó el ServerSocket");
            while (true)
            {
                try{
                    server = serverSocket.accept();
                    //System.out.println("Ya aceptó algo el servidor");
                    DataInputStream inputStream = new DataInputStream(server.getInputStream());
                    txtMessages.setText(txtMessages.getText() + "\n" + inputStream.readUTF());
                    server.close();
                }
                catch (IOException ex){
                    System.out.println (ex.getMessage());
                }
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
 
//    @Override
//    public void run() {
//        ServerSocket serverSocket;
//        Socket server;
//        try {
//            serverSocket = new ServerSocket(5555);
//            //System.out.println("Ya creó el ServerSocket");
//            while (true)
//            {
//                try{
//                    server = serverSocket.accept();
//                    //System.out.println("Ya aceptó algo el servidor");
//                    DataInputStream inputStream = new DataInputStream(server.getInputStream());
//                    txtMessages.setText(txtMessages.getText() + "\n" + inputStream.readUTF());
//                    System.out.println(inputStream.readUTF());
//                    server.close();
//                }
//                catch (IOException ex){
//                    System.out.println (ex.getMessage());
//                }
//            }
//        } catch (IOException ex) {
//            System.out.println(ex.getMessage());
//        }
//    }    
}

class MyThread extends Thread 
{
    public MyThread(String name){
        super(name);
        start();
    }

    @Override
    public void run() {
        ServerSocket serverSocket;
        Socket server;
        try {
            serverSocket = new ServerSocket(5555);
            //System.out.println("Ya creó el ServerSocket");
            while (true)
            {
                try{
                    server = serverSocket.accept();
                    //System.out.println("Ya aceptó algo el servidor");
                    DataInputStream inputStream = new DataInputStream(server.getInputStream());
                    txtMessages.setText(txtMessages.getText() + "\n" + inputStream.readUTF());
                    System.out.println(inputStream.readUTF());
                    server.close();
                }
                catch (IOException ex){
                    System.out.println (ex.getMessage());
                }
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }    
}